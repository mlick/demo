### 此项目为demo集合,详细文档请点击 [framework-boot](https://gitee.com/sesamekim/framework-boot)
### 说明
- simple_web : 一个最简单的web项目 demo
- ssm : web整合mybatis后的dome
- cache : 缓存的使用,结合数据库查询
- lock : 分布式锁
- serial-no : 序列号
- distributed-tx : 分布式事物
- spring-cloud
```
eureka : 注册中心
comsumer: 消费者
producer:生产者
```