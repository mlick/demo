/**
 * web 界面上的公共javascript封装
 *
 * web.url_replace : url 替换
 * web.ajax  :  异步请求
 * web.ajax_sync :  同步请求
 * web.page_query :  ist 界面上的分页查询
 * web.delete_select : list 界面上的删除
 * web.delete_select_all : list 界面上的全部删除
 *
 * @author 01249527-王江海
 * @date 2017/12/7 11:07
 */
layui.define(['element', 'form', 'layer', 'table', 'laypage'], function (exports) {
    var element = layui.element;
    var form = layui.form;
    var layer = layui.layer;
    var table = layui.table;
    var laypage = layui.laypage;
    var $ = layui.jquery;

    var obj = {
        /**
         * url 替换
         */
        url_replace: function (url) {
            url = basePath + '/page/' + url.replace(new RegExp('/', 'g'), pageReplace);
            return url;
        }
        /**
         * 异步请求
         */
        , ajax: function (url, type, data, successfn) {
            web_ajax(url, type, data, successfn, true);
        }

        /**
         * 同步请求 : 会锁住页面==> 页面下面的 javascript 必须等待回调函数处理完 才能执行
         */
        , ajax_sync: function (url, type, data, successfn) {
            web_ajax(url, type, data, successfn, false);
        }

        /**
         * list 界面上的分页查询
         * @param form_id   分页表单的id
         * @param page_id  分页信息显示的 div id
         * @param refreshdatafn  请求成功(data.success==true)  时的回调, 传入list
         */
        , page_query: function (form_id, page_id, refreshdatafn) {
            if (form_id == null || form_id == "" || form_id == "#") {
                form_id = "#query_from";
            }
            if (page_id == null || page_id == "" || page_id == "#") {
                page_id = "#page_div";
            }
            if ($("#pageNum").val() == "") {
                $("#pageNum").val(1);
            }
            if ($("#pageSize").val() == "") {
                $("#pageSize").val(defaultPageSize);
            }
            web_page_query(form_id, page_id, refreshdatafn);
        }

        /**
         *  list 界面上的删除
         * @param contro_path  controller 的路径,
         * @param ids  删除的id  类似 1,2,3
         * @param delfn 删除成功后的回调
         */
        , delete_select: function (contro_path, ids, delfn) {
            web_delete_select(contro_path, ids, delfn);
        }

        /**
         *  list 界面上的全部删除
         * @param contro_path  controller 的路径,
         * @param table_list_id   table 的id , 用于获取选中的行
         * @param delfn  删除成功后的回调
         */
        , delete_select_all: function (contro_path, table_list_id, delfn) {
            web_delete_select_all(contro_path, table_list_id, delfn);
        }

    };

    /**
     * ajax  请求封装
     */
    function web_ajax(url, type, data, successfn, asyncType) {
        type = (type == null || type == "" || typeof(type) == "undefined") ? "post" : type;
        data = (data == null || data == "" || typeof(data) == "undefined") ? {"date": new Date().getTime()} : data;
        $.ajax({
            type: type,
            async: asyncType,
            data: data,
            url: url,
            success: function (d) {
                if (d.success) {
                    successfn(d);
                } else {
                    var icon = 2;
                    if (d.errorCode = 2000) {
                        icon = 2;
                    }
                    layer.alert(d.message, {
                        icon: icon
                    });
                }
            },
            error: function (e) {
                layer.alert('系统错误,请重试', {
                    icon: icon
                });
            }
        });
    }

    /**
     *  分页查询
     */
    function web_page_query(form_id, page_id, refreshdatafn) {

        var url = $(form_id).attr("action");
        var data = $(form_id).serialize();
        var type = $(form_id).attr("method");
        web_ajax(url, type, data, function (data) {
            refreshdatafn(data.result);
            var page = data.page;
            if (page != null) {
                $(page_id).show();
                laypage.render({
                    elem: 'page_div'
                    , curr: page.pageNum
                    , count: page.total
                    , limit: page.pageSize
                    , layout: ['prev', 'page', 'next', 'limit', 'skip', 'count']
                    , jump: function (obj, first) {
                        if (!first) {
                            var pageNum = $("#pageNum").val();
                            var pageSize = $("#pageSize").val();
                            // 防止无限刷新 , 只有监听到的页面index 和当前页不一样是才出发分页查询
                            if (obj.curr != pageNum || obj.limit != pageSize) {
                                $("#pageNum").val(obj.curr);
                                $("#pageSize").val(obj.limit);
                                web_page_query(form_id, page_id, refreshdatafn);
                            }
                        }

                    }
                });
            } else {
                $(page_id).hide();
            }
        }, true);
    }

    /**
     * list 界面上的删除
     */
    function web_delete_select(contro_path, ids, delfn) {
        layer.confirm('确定要删除么?', {
            icon: 3,
            title: '提示'
        }, function (index) {
            layer.close(index);
            web_ajax(basePath + contro_path + "/delete", 'post', {ids: ids}, function (result) {
                layer.alert("删除成功", {
                    icon: 1
                }, function (index1) {
                    layer.close(index1);
                    delfn();// 删除成功后的回调
                });
            }, true);
        });

    }

    /**
     * list 界面上的全部删除
     */
    function web_delete_select_all(contro_path, table_list_id, delfn) {
        var checkStatus = table.checkStatus(table_list_id);
        var length = checkStatus.data.length;//获取选中行数量
        if (length == 0) {
            layer.alert("请至少选择一条记录", {
                icon: 2
            });
            return;
        }
        var list = checkStatus.data; //获取选中行的数据
        var ids = new Array();
        for (var i = 0; i < length; i++) {
            ids[i] = list[i].id;
        }
        web_delete_select(contro_path, ids.toString(), delfn);
    }

    exports('web', obj);
});
