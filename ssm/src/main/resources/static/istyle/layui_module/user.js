/**
 *用户相关信息
 * @author 01249527-王江海
 * @date 2017/12/7 11:07
 */
layui.define(['layer', 'web'], function (exports) {
    var layer = layui.layer;
    var web = layui.web;
    var $ = layui.jquery;

    var obj = {
        getUser: function () {
            if(window.user == null){
                getUser();
            }
            return window.user;
        }
        ,getRole:function () {
            if(window.role == null){
                getUser();
            }
            return window.role;
        }
    };

    function getUser() {
        web.ajax_sync(basePath + "/login/getUserInfo", 'post', {}, function (data) {
            user = data.result.user;
            role = data.result.role;
        });
        window.user = user;
        window.role = role;
    }

    exports('user', obj);
});
