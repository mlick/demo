package com.sesame;

import com.sesame.framework.web.context.SpringContextUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * 项目启动类
 */
@ServletComponentScan
@SpringBootApplication
public class AppSSM {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(AppSSM.class, args);

        SpringContextUtil.setApplicationContext(context);
        SpringContextUtil.println();

    }

}
