package com.sesame.student.dao;

import org.apache.ibatis.annotations.Mapper;
import com.sesame.student.bean.Student;

import java.util.List;

/**
 * StudentDao
 * @author admin
 * @date 2017-12-24 18:09:00
 * @Description: 学生表
 */
 @Mapper
public interface StudentDao {

	/** 新增  */
	int insert(Student bean);
	
	/** 删除  */
	int delete(String id);
	
	/** 修改  */
	int update(Student bean);
	
	/** 查询所有 */
	List<Student> selectList(Student bean);
	
	/** 查询单个,返回bean */
	Student search(Student bean);
	

}
