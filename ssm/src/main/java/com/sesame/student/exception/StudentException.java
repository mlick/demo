package com.sesame.student.exception;

import com.sesame.framework.exception.BusinessException;

public class StudentException extends BusinessException {

    private static final long serialVersionUID = 3827413256427200111L;

    public static final String NOT_NAME = "姓名不能为空";
    public static final String ORDER_NON = "单号不存在";

    public StudentException() {
    }

    public StudentException(String msg) {
        super(msg);
    }

    public StudentException(String code, String msg) {
        super(msg);
        this.errCode = code;
    }

}
