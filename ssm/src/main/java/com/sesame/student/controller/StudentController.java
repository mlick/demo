package com.sesame.student.controller;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sesame.framework.entity.GPage;
import com.sesame.framework.mybatis.uitl.PageUtil;
import com.sesame.framework.utils.StringUtil;
import com.sesame.framework.web.controller.AbstractWebController;
import com.sesame.framework.web.response.Response;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sesame.student.bean.Student;
import com.sesame.student.service.StudentService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * StudentController
 * @author admin
 * @date 2017-12-24 18:09:00
 * @Description: 学生表
 */
@CommonsLog
@RestController
@RequestMapping("/student")
public class StudentController extends AbstractWebController {

	@Resource
	private StudentService studentService;

	/**
	 * 查询所有
	  * @author admin
      * @date 2017-12-24 18:09:00
      * @Description:
	 */
	@RequestMapping("/listAll")
	public Response listAll(Student bean) {

		List<Student> list = studentService.searchList(bean);

		return returnSuccess(list);
	}
	
	/**
	 * 分页查询
	 * @author admin
     * @date 2017-12-24 18:09:00
     * @ pageNum : 页码,默认1
     * @ pageSize :  页面大小
	 */
	@RequestMapping("/listPage")
	public Response listPage(Student bean, GPage gPage) {

		Page<Student> pages = PageHelper.startPage(gPage.getPageNum(), gPage.getPageSize())
                        .doSelectPage(() -> studentService.searchList(bean));
        List<Student> list = pages.getResult();
		
		return returnSuccess(list, PageUtil.recount(gPage, pages));
	}
	
	/**
	 * 保存
	 * @author admin
     * @date 2017-12-24 18:09:00
     * @Description:	
	 */
	@RequestMapping(value = "/save",method = {RequestMethod.POST})
	public Response save(Student bean) {
		//IUser user = UserContext.getUserContext().getUser();
		int res = 0;
        if (StringUtil.isNotEmpty(bean.getId())) {
            // 主键不为空, 修改数据
            bean.initUpdate("");
            res = studentService.update(bean);
        } else {
            //主键为空, 新增数据
            bean.initCreateAndId("");
            res = studentService.add(bean);
        }
		return returnSuccess(res);
	}
		
	/**
	 * 删除
	 * @author admin
     * @date 2017-12-24 18:09:00
     * @Description:	
	 */
	@RequestMapping("/delete")
	public Response delete(String ids,HttpServletRequest request) {
		int res = studentService.delete(ids);
		return returnSuccess(res);
	}
		
	/**
	 * 查询详情
	 * @author admin
     * @date 2017-12-24 18:09:00
     * @Description:	
	 */
	@RequestMapping("/searchDetail")
	public Response searchDetail(Student bean,HttpServletRequest request) {
		bean = studentService.search(bean);
		return returnSuccess(bean);
	}
		
}
