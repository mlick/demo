package com.sesame.student.service;

import com.sesame.framework.exception.BusinessException;
import com.sesame.framework.utils.StringUtil;
import com.sesame.student.exception.StudentException;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sesame.student.bean.Student;
import com.sesame.student.dao.StudentDao;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * StudentService
 *
 * @author admin
 * @date 2017-12-24 18:09:00
 * @Description: 学生表
 */
@CommonsLog
@Service
public class StudentService {

    @SuppressWarnings("all")
    @Resource
    private StudentDao studentDao;

    /**
     * 查询所有
     *
     * @author admin
     * @date 2017-12-24 18:09:00
     * @Description: 分页时要注意
     */
    public List<Student> searchList(Student bean) {

        List<Student> list = studentDao.selectList(bean);

//		if(list.size()>0){
//			throw new BusinessException("I am bug!!!");
//		}
//        if (list.size() > 0) {
//            throw new StudentException(StudentException.NOT_NAME);
//        }

        return list;
    }

    /**
     * 新增
     *
     * @author admin
     * @date 2017-12-24 18:09:00
     * @Description:
     */
    @Transactional(rollbackFor = Exception.class)
    public int add(Student bean) {

        int res = studentDao.insert(bean);

        return res;
    }

    /**
     * 修改
     *
     * @author admin
     * @date 2017-12-24 18:09:00
     * @Description:
     */
    @Transactional(rollbackFor = Exception.class)
    public int update(Student bean) {

        int res = studentDao.update(bean);

        return res;
    }

    /**
     * 删除
     *
     * @author admin
     * @date 2017-12-24 18:09:00
     * @Description:
     */
    @Transactional(rollbackFor = Exception.class)
    public int delete(String ids) {

        List<String> list_id = Stream.of(ids.split(",")).map(String::trim).distinct().filter(StringUtil::isNotEmpty).collect(Collectors.toList());
        list_id.stream().forEach(this::deleteById);

        return list_id.size();
    }

    @Transactional(rollbackFor = Exception.class)
    public void deleteById(String id) {
        //物理删除
        studentDao.delete(id);

        //逻辑删除
//    Student bean = new Student();
//    	bean.setId(id);
//    	bean.setActive(GData.BOOLEAN.NO);
//    	bean.setUpdateTime(new Date());
//    	update(bean);
    }

    /**
     * 查询--返回bean
     *
     * @author admin
     * @date 2017-12-24 18:09:00
     * @Description:
     */
    public Student search(Student bean) {

        return studentDao.search(bean);
    }

}
