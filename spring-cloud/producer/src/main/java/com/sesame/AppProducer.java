package com.sesame;

import com.sesame.framework.web.context.SpringContextUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.ConfigurableApplicationContext;

//@EnableRedisHttpSession
@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
public class AppProducer {

	public static void main(String[] arr) {
		
		ConfigurableApplicationContext context = SpringApplication.run(AppProducer.class, arr);

		SpringContextUtil.setApplicationContext(context);
		SpringContextUtil.println();

		Runtime.getRuntime().addShutdownHook(new Thread(()->{
			System.out.println(" 我停止了...");
		}));
	}
}