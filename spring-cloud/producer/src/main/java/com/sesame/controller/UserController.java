package com.sesame.controller;

import com.sesame.entity.User;
import com.sesame.framework.entity.GMap;
import com.sesame.framework.utils.DateUtils;
import com.sesame.framework.web.controller.AbstractWebController;
import com.sesame.framework.web.response.Response;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@CommonsLog
@RestController
@RequestMapping("/user")
public class UserController extends AbstractWebController {

    @Resource
    private Environment environment;

    /**
     * http://localhost:8050/producer/user/test
     */
    @RequestMapping("test")
    public Response test(String value, HttpServletRequest request) {
        GMap params = GMap.newMap();
        params.putAction("value", value);
        params.putAction("requestType", request.getMethod());
        params.putAction("datetime", DateUtils.convert(new Date()));

        String info =  environment.getProperty("server.port") + environment.getProperty("server.context-path");
        params.putAction("info",info);

//        try {
//            TimeUnit.SECONDS.sleep(5);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        return returnSuccess(params);
    }

    /**
     * http://localhost:8050/producer/user/getUser
     */
    @RequestMapping("getUser")
    public Response getUser() {
        return returnSuccess(new User("张三", 1));
    }
    /**
     * http://localhost:8050/producer/user/getList
     */
    @RequestMapping("getList")
    public Response getList() {
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        List<User> list = Arrays.asList(new User("张三", 1), new User("李四", 2));
        return returnSuccess(list);
    }
    /**
     * http://localhost:8050/producer/user/getListPage
     */
    @RequestMapping("getListPage")
    public Response getListPage() {
        List<User> list = Arrays.asList(new User("张三", 1), new User("李四", 2));
        //Page page = new Page(2, 10);
        return returnSuccess(list);
    }





}