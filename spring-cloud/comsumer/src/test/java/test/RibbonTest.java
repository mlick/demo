package com.sesame.test;

import com.github.kevinsawicki.http.HttpRequest;
import com.sesame.AppComsume;
import com.sesame.framework.entity.GMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = AppComsume.class)
public class RibbonTest {

    private static final String url_test = "http://eureka-producer/producer/user/test";
    private static GMap params = null;

    @Before
    public void before() {
        params = GMap.newMap();
        params.putAction("value", "init_val_张");
    }

    @Autowired
    public RestTemplate restTemplate;

    @Test
    public void test_get() {
        String res = "";

        res = restTemplate.getForObject(url_test + "?value={value}", String.class, params);

        System.out.println(">>>>>>>>>>");
        System.out.println(res);
        System.out.println("<<<<<<<<<<<<<");
    }


}