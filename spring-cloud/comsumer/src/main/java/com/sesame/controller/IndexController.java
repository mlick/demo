package com.sesame.controller;

import com.sesame.framework.entity.GMap;
import com.sesame.framework.web.context.SpringContextUtil;
import com.sesame.framework.web.controller.AbstractWebController;
import com.sesame.framework.web.response.Response;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@CommonsLog
@Controller
@RequestMapping("/index")
public class IndexController extends AbstractWebController {

    static final String cookie_name = "cookie_name";
    static final int time = 60;//秒

    /**
     * http://localhost:2002/api-c/comsumer/index/test
     */
    @RequestMapping("/test")
    @ResponseBody
    public Response test(String id, String name, String zuulSessionId, HttpServletRequest request, ModelMap modelMap) {

        GMap params = GMap.newMap();
        modelMap.put("success", true);
        params.putAction("sessionId", request.getSession().getId());
        params.putAction("zuulSessionId", zuulSessionId);
        params.putAction("servlet",SpringContextUtil.getAttributeValue("server.context-path")+":"+SpringContextUtil.getAttributeValue("server.port"));

        return returnSuccess(params);
    }

}