package com.sesame.controller;

import com.sesame.entity.User;
import com.sesame.framework.web.controller.AbstractWebController;
import com.sesame.framework.web.response.Response;
import com.sesame.service.FeignService;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CommonsLog
@RestController
@RequestMapping("/feign")
public class FeignController extends AbstractWebController {

    @Autowired
    FeignService feignService;

    /**
     * http://localhost:8060/comsumer/feign/test
     */
    @GetMapping("test")
    public Response test() {
        return feignService.test("init_张山");
    }
    /**
     * http://localhost:8060/comsumer/feign/getUser
     */
    @GetMapping("getUser")
    public User getUser() {
        Response<User> result = feignService.getUser();
//        if(result!=null){
//            throw new BusinessException("8888","参数错误11");
//        }
        return result.getResult();
    }

    /**
     * http://localhost:8060/comsumer/feign/getList
     */
    @GetMapping("getList")
    public List<User> getList() {
        Response<List<User>> result = feignService.getList();
        result.getResult().forEach(bean->{
            System.out.println(bean.getName()+" : "+bean.getAge());
        });
        return result.getResult();
    }
    @GetMapping("list")
    public Response list() {
        Response<List<User>> result = feignService.getList();
        return result;
    }

    /**
     * http://localhost:8060/comsumer/feign/getListPage
     */
    @GetMapping("getListPage")
    public Response getListPage() {
        Response<List<User>> result = feignService.getListPage();

        List<User> list = result.getResult();
        list.forEach(bean->{
            System.out.println(bean.getName()+" : "+bean.getAge());
        });
        System.out.println("*******************");

        return result;
    }
}