package com.sesame.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.sesame.framework.entity.GMap;
import com.sesame.framework.web.controller.AbstractWebController;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@CommonsLog
@RestController
@RequestMapping("/ribbon")
public class RibbonController extends AbstractWebController {

    private static final String url_test = "http://eureka-producer/producer/user/test";
//    http://localhost:8050/producer/user/test
    private static GMap params = null;

    static {
        params = GMap.newMap();
        params.putAction("value", "init_val_张");
    }

    @Autowired
    public RestTemplate restTemplate;
    @Autowired
    public ConsumerService consumerService;

    /**
     * http://localhost:8060/comsumer/ribbon/test_get
     */
    @RequestMapping("test_get")
    public String test_get() {
        String res = consumerService.consumer();
        return res;
    }
    /**
     * http://localhost:8060/comsumer/ribbon/test_post
     */
    @RequestMapping("test_post")
    public String test_post() {
        MultiValueMap<String, String> paramMap = new LinkedMultiValueMap<>();
        paramMap.add("value", "init_val_张");
        String res = restTemplate.postForObject(url_test, paramMap, String.class);
        return res;
    }
}
@Service
class ConsumerService {
    String url_test = "http://eureka-producer/producer/user/test";

    @Autowired
    RestTemplate restTemplate;
    @HystrixCommand(fallbackMethod = "fallback")
    public String consumer() {
        GMap  params = GMap.newMap();
        params.putAction("value", "init_val_张");
        String res = restTemplate.getForObject(url_test + "?value={value}", String.class, params);
        return  res;
    }
    public String fallback() {
        return "fallback";
    }
}