package com.sesame.service;

import com.sesame.entity.User;
import com.sesame.framework.web.response.Response;
import com.sesame.framework.web.response.ResponseFactory;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * feign 失败的原因
 * https://www.cnblogs.com/devzxd/p/feign-hystrix-problem.html
 */
@Component
public class FeignServiceHystrix implements FeignService {
    Response response = Response.create();

    @Override
    public Response test(String value) {
        return null;
    }

    @Override
    public Response<User> getUser() {
        return ResponseFactory.feignException("getUser");
    }

    @Override
    public Response<List<User>> getList() {

        return ResponseFactory.feignException("getList");
    }

    @Override
    public Response<List<User>> getListPage() {
        return null;
    }
}