package com.sesame.service;


import com.sesame.entity.User;
import com.sesame.framework.web.response.Response;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

import static com.sesame.service.FeignService.APP_NAME;

@FeignClient(name=APP_NAME, fallback = FeignServiceHystrix.class)
public interface FeignService {

    static final String APP_NAME = "eureka-producer";
    static final String SERVICE_NAME = "producer";

    @RequestMapping(value = SERVICE_NAME + "/user/test", method = RequestMethod.POST)
    Response test(@RequestParam("value") String value);

    @GetMapping(SERVICE_NAME + "/user/getUser")
    Response<User> getUser();

    @GetMapping(SERVICE_NAME + "/user/getList")
    Response<List<User>> getList();

    @GetMapping(SERVICE_NAME + "/user/getListPage")
    Response<List<User>> getListPage();


}