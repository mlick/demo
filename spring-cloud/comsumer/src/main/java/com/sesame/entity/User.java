package com.sesame.entity;

import lombok.Data;

@Data
public class User implements  java.io.Serializable {
    private String name;
    private int age;

    public User() {
    }

    public User(String name, int age) {
        this.name = name;
        this.age = age;
    }
}