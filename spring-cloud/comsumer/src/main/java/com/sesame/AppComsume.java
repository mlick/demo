package com.sesame;

import com.sesame.framework.web.context.SpringContextUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.ConfigurableApplicationContext;

@EnableFeignClients
@EnableCircuitBreaker
@EnableDiscoveryClient
@SpringBootApplication
public class AppComsume {

	public static void main(String[] arr) {
		
		ConfigurableApplicationContext context = SpringApplication.run(AppComsume.class, arr);

		SpringContextUtil.setApplicationContext(context);
		SpringContextUtil.println();

		Runtime.getRuntime().addShutdownHook(new Thread(()->{
			System.out.println(" 我停止了...");
		}));
	}
}