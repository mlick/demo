package com.sesame;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class AppEureka {
	public static void main(String[] args) {
		new SpringApplicationBuilder(AppEureka.class).web(true).run(args);
	}
}