package com.sesame.config;

import com.sesame.framework.serial.define.SerialNumberRule;

/**
 * 序列号配置类,
 * 里面的code要保证系统唯一
 */
public class MySerialNumber extends SerialNumberRule {

    public static final SerialNumberRule QAS_HEADLESS_CODE =new MySerialNumber
            ("微信", "WX", false, true, "yyMMdd",
                    false, "", true, "QAW", true,
                    true, 6, false, "");

    public static final SerialNumberRule QAS_ARBITRATION_CODE =new MySerialNumber
            ("支付宝", "ZFB", false, true, "yyMMdd",
                    false, "", true, "QAZ", true,
                    true, 6, false, "");


    private MySerialNumber(String name, String code, boolean needParams, boolean needTime, String timeFormat, boolean needDelimiter, String delimiter, boolean needLetterPrefix, String letterPrefix, boolean needNumber, boolean fixedNumLen, int numLen, boolean needLetterSuffix, String letterSuffix) {
        super(name, code, needParams, needTime, timeFormat, needDelimiter, delimiter, needLetterPrefix, letterPrefix, needNumber, fixedNumLen, numLen, needLetterSuffix, letterSuffix);
    }
}