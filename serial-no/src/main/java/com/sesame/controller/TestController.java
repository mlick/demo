package com.sesame.controller;

import com.sesame.config.MySerialNumber;
import com.sesame.framework.lock.service.DistributedLocker;
import com.sesame.framework.serial.define.ISerialNumberService;
import com.sesame.framework.web.controller.AbstractWebController;
import com.sesame.framework.web.response.Response;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

@CommonsLog
@RestController
@RequestMapping("/test")
public class TestController extends AbstractWebController {

    @Autowired
    private DistributedLocker locker;
    @Autowired
    private ISerialNumberService serialNumberService;


    @RequestMapping("/getNo1")
    public Response getNo1() {

        String no1 = serialNumberService.generateSerialNumber(MySerialNumber.QAS_HEADLESS_CODE);
        log.info("no1:" + no1);

        return returnSuccess(Arrays.asList(no1));
    }

    @RequestMapping("/getNo2")
    public Response getNo2() {

        String no2 = serialNumberService.generateSerialNumber(MySerialNumber.QAS_ARBITRATION_CODE);
        log.info("no2:" + no2);

        return returnSuccess(Arrays.asList(no2));
    }


}
