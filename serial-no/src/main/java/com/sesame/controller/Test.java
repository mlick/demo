package com.sesame.controller;

import com.github.kevinsawicki.http.HttpRequest;

public class Test {
    public static void main(String[] args) {
        String url1 = "http://localhost:8084/serial/test/getNo1";
        String url2 = "http://localhost:8084/serial/test/getNo2";
        for (int i = 0; i < 3; i++) {
            new Thread(() -> {
                HttpRequest.get(url1).body();
                sleep();
            }).start();
            new Thread(() -> {
                HttpRequest.get(url2).body();
                sleep();
            }).start();
        }
    }
    public static void sleep(){
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
