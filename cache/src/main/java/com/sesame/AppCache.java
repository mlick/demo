package com.sesame;

import com.sesame.framework.web.context.SpringContextUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * 项目启动类
 */
@SpringBootApplication
public class AppCache {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(AppCache.class, args);

        SpringContextUtil.setApplicationContext(context);
        SpringContextUtil.println();

    }

}
