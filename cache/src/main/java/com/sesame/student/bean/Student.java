package com.sesame.student.bean;

import com.sesame.framework.entity.BaseEntity;
import lombok.Data;
import java.util.Date;
import java.sql.*;

import java.math.*;

/**
 * Student
 * @author admin
 * @date 2017-12-24 18:09:00
 * @Description: 学生表
 */
@Data
public class Student extends BaseEntity {
	private static final long serialVersionUID = 1L;

	private String name;//姓名
	private Integer sex;//性别
	private String email;//邮箱
	private String phone;//手机
	private String addr;//地址
	private Date createtime;//创建时间

    // not database field ...
	
}