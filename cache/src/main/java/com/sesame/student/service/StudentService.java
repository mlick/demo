package com.sesame.student.service;

import com.sesame.framework.cache.CacheManager;
import com.sesame.framework.cache.ICache;
import com.sesame.framework.utils.StringUtil;
import com.sesame.student.bean.Student;
import com.sesame.student.cache.StudentCache;
import com.sesame.student.cache.StudentListCache;
import com.sesame.student.dao.StudentDao;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * StudentService
 *
 * @author admin
 * @date 2017-12-24 18:09:00
 * @Description: 学生表
 */
@CommonsLog
@Service
public class StudentService {

    @SuppressWarnings("all")
    @Resource
    private StudentDao studentDao;

    /**
     * 查询所有
     *
     * @author admin
     * @date 2017-12-24 18:09:00
     * @Description: 分页时要注意
     */
    public List<Student> searchList(Student bean) {

//        List<Student> list = studentDao.selectList(bean);
        ICache<String, Student> cache = getStudentListCache();
        Map<String, Student> map = cache.get();
        List<Student> list = new ArrayList<>();
        list.addAll(map.values());
        list.sort((s1, s2) -> Integer.parseInt(s1.getId()) - Integer.parseInt(s2.getId()));
        return list;
    }

    /**
     * 新增
     *
     * @author admin
     * @date 2017-12-24 18:09:00
     * @Description:
     */
    @Transactional(rollbackFor = Exception.class)
    public int add(Student bean) {

        int res = studentDao.insert(bean);

        return res;
    }

    /**
     * 修改
     *
     * @author admin
     * @date 2017-12-24 18:09:00
     * @Description:
     */
    @Transactional(rollbackFor = Exception.class)
    public int update(Student bean) {

        int res = studentDao.update(bean);

        return res;
    }

    /**
     * 删除
     *
     * @author admin
     * @date 2017-12-24 18:09:00
     * @Description:
     */
    @Transactional(rollbackFor = Exception.class)
    public int delete(String ids) {

        List<String> list_id = Stream.of(ids.split(",")).map(String::trim).distinct().filter(StringUtil::isNotEmpty).collect(Collectors.toList());
        list_id.stream().forEach(this::deleteById);

        return list_id.size();
    }

    @Transactional(rollbackFor = Exception.class)
    public void deleteById(String id) {
        //物理删除
        studentDao.delete(id);

        //逻辑删除
//    Student bean = new Student();
//    	bean.setId(id);
//    	bean.setActive(GData.BOOLEAN.NO);
//    	bean.setUpdateTime(new Date());
//    	update(bean);
    }

    /**
     * 查询--返回bean
     *
     * @author admin
     * @date 2017-12-24 18:09:00
     * @Description:
     */
    public Student search(Student bean) {

        Student student = getStudentCache().get(bean.getId());

        //return studentDao.search(bean);
        return student;
    }

    private ICache<String, Student> getStudentCache() {
        ICache<String, Student> cache = CacheManager.getInstance().getCache(StudentCache.UUID);
        return cache;
    }

    private ICache<String, Student> getStudentListCache() {
        ICache cache = CacheManager.getInstance().getCache(StudentListCache.UUID);
        return cache;
    }
}
