package com.sesame.student.cache;

import com.sesame.framework.cache.provider.IBatchCacheProvider;
import com.sesame.student.bean.Student;
import com.sesame.student.dao.StudentDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * StudentListProvider
 *
 * @author 01249527-王江海
 * @date 2017-11-28 10:37
 * @classpath com.sesame.student.cache.StudentListProvider
 * @Description:
 */
@Component
public class StudentListProvider implements IBatchCacheProvider<String, Student> {

    @SuppressWarnings("all")
    @Autowired
    public StudentDao studentDao;

    @Override
    public Map<String, Student> get() {
        Map<String, Student> map = new HashMap<>();
        List<Student> students = studentDao.selectList(new Student());
        for (Student s : students) {
            map.put(s.getId(), s);
        }
        return map;
    }

    @Override
    public Date getLastModifyTime() {
        return new Date();
    }
}