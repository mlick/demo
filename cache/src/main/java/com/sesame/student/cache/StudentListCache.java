package com.sesame.student.cache;

import com.sesame.framework.cache.redis.DefaultStrongRedisCache;
import com.sesame.framework.cache.redis.storage.RedisCacheStorage;
import com.sesame.student.bean.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 定时刷新缓存
 */
@Component
public class StudentListCache extends DefaultStrongRedisCache<String,Student> {
    public static final String UUID = StudentListCache.class.getName();
    @Override
    public String getUUID() {
        return UUID;
    }

    @Autowired
    public void setCacheProvider(StudentListProvider cacheProvider) {
        super.setCacheProvider(cacheProvider);
    }

    @Override
    @Autowired
    public void setCacheStorage(RedisCacheStorage cacheStorage) {
        super.setCacheStorage(cacheStorage);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        setInterval(3000);
        super.afterPropertiesSet();
    }
}