package com.sesame.student.cache;


import com.sesame.framework.cache.redis.DefaultTTLRedisCache;
import com.sesame.framework.cache.redis.storage.RedisCacheStorage;
import com.sesame.student.bean.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 定时失效缓存
 */
@Component
public class StudentCache extends DefaultTTLRedisCache<Student> {
    public static final String UUID = Student.class.getName();
    @Override
    public String getUUID() {
        return UUID;
    }

    //注入缓存存储器
    @Autowired
    @Override
    public void setCacheStorage(RedisCacheStorage<String, Student> cacheStorage) {
        super.setCacheStorage(cacheStorage);
    }

    @Autowired
    public void setCacheProvider(StudentCodeProvider cacheProvider) {
        super.setCacheProvider(cacheProvider);
    }

}