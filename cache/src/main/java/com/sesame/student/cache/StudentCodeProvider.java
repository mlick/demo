package com.sesame.student.cache;

import com.sesame.framework.cache.provider.ITTLCacheProvider;
import com.sesame.student.bean.Student;
import com.sesame.student.dao.StudentDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StudentCodeProvider  implements ITTLCacheProvider<Student> {

    @SuppressWarnings("all")
    @Autowired
    public StudentDao studentDao;

    @Override
    public Student get(String key) {
        Student bean = new Student();
        bean.setId(key);

        return studentDao.search(bean);
    }
}