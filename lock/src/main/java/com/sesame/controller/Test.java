package com.sesame.controller;

import com.github.kevinsawicki.http.HttpRequest;

public class Test {
    public static void main(String[] args) {
        String url = "http://localhost:8083/lock/test/readDb";
        for (int i = 0; i < 5; i++) {
            new Thread(() -> {
                HttpRequest.get(url).body();
            }).start();
        }
    }
}
