package com.sesame.txtest.bean;

import com.sesame.framework.entity.BaseEntity;
import lombok.Data;
import java.util.Date;
import java.sql.*;

import java.math.*;

/**
 * TxTest
 * @author admin
 * @date 2017-12-21 16:47:05
 * @Description: 分布式事物测试表
 */
@Data
public class TxTest extends BaseEntity {
	private static final long serialVersionUID = 1L;

	private String code;//编号
	private String value;//值

    // not database field ...
	
}