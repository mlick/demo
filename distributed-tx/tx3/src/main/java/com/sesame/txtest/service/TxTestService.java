package com.sesame.txtest.service;

import com.sesame.framework.exception.BusinessException;
import com.sesame.txtest.bean.TxTest;
import com.sesame.txtest.dao.TxTestDao;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * TxTestService
 *
 * @author admin
 * @date 2017-12-21 16:47:05
 * @Description: 分布式事物测试表
 */
@CommonsLog
@Service
public class TxTestService {

    @SuppressWarnings("all")
    @Resource
    private TxTestDao txTestDao;

    /**
     * 查询所有
     *
     * @author admin
     * @date 2017-12-21 16:47:05
     * @Description: 分页时要注意
     */
    public List<TxTest> searchList(TxTest bean) {

        List<TxTest> list = txTestDao.selectList(bean);

        return list;
    }

    /**
     * 新增
     *
     * @author admin
     * @date 2017-12-21 16:47:05
     * @Description:
     */
    @Transactional(rollbackFor = Exception.class)
    public int add(TxTest bean) {

        int res = txTestDao.insert(bean);

        return res;
    }

    /**
     * c  参与方  localhost:8087
     * 抛出异常,
     */
    @Transactional(rollbackFor = Exception.class)
    public int update3(TxTest bean) {

        int res = txTestDao.update(bean);
        if (res == 1) {
            throw new BusinessException("异常测试 c");
        }

        return res;
    }


}
