package com.sesame.txtest.controller;

import com.sesame.framework.web.controller.AbstractWebController;
import com.sesame.framework.web.response.Response;
import com.sesame.txtest.bean.TxTest;
import com.sesame.txtest.service.TxTestService;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * TxTestController
 *
 * @author admin
 * @date 2017-12-21 16:47:05
 * @Description: 分布式事物测试表
 */
@CommonsLog
@RestController
@RequestMapping("/txtest")
public class TxTestController extends AbstractWebController {

    @Resource
    private TxTestService txTestService;

    /**
     * 查询所有
     *
     * @author admin
     * @date 2017-12-21 16:47:05
     * @Description:
     */
    @RequestMapping("/listAll")
    public Response listAll(TxTest bean) {

        List<TxTest> list = txTestService.searchList(bean);

        return returnSuccess(list);
    }


    @RequestMapping(value = "/update3")
    public Response update3(TxTest bean) {

        int res = txTestService.update3(bean);

        return returnSuccess(res);
    }

}
