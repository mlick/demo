package com.sesame;

import com.alibaba.druid.pool.DruidDataSource;
import com.codingapi.tx.datasource.relational.LCNTransactionDataSource;
import com.codingapi.tx.springcloud.feign.TransactionRestTemplateInterceptor;
import com.sesame.framework.web.context.SpringContextUtil;
import feign.RequestInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;


@EnableFeignClients
@EnableCircuitBreaker
@EnableDiscoveryClient
@SpringBootApplication
public class AppTX1 {

	public static void main(String[] arr) {
		ConfigurableApplicationContext context = SpringApplication.run(AppTX1.class, arr);

		SpringContextUtil.setApplicationContext(context);
		SpringContextUtil.printlnInfo();
	}
}