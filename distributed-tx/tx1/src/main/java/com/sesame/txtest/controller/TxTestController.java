package com.sesame.txtest.controller;

import com.sesame.framework.utils.StringUtil;
import com.sesame.framework.web.controller.AbstractWebController;
import com.sesame.framework.web.response.Response;
import com.sesame.txtest.bean.TxTest;
import com.sesame.txtest.feign.FeignServiceTx2;
import com.sesame.txtest.service.TxTestService;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * TxTestController
 *
 * @author admin
 * @date 2017-12-21 16:47:05
 * @Description: 分布式事物测试表
 */
@CommonsLog
@RestController
@RequestMapping("/txtest")
public class TxTestController extends AbstractWebController {

    @Resource
    private TxTestService txTestService;
    @Autowired
    private FeignServiceTx2 feignService;

    /**
     * 查询所有
     *
     * @author admin
     * @date 2017-12-21 16:47:05
     * @Description:
     * http://localhost:8085/tx1/txtest/listAll
     */
    @RequestMapping("/listAll")
    public Response listAll(TxTest bean) {

        Response<List<TxTest>> list1 = feignService.getList();
        //List<TxTest> list = list1.getResult();

        return list1;
    }

    /**
     * 保存
     *
     * @author admin
     * @date 2017-12-21 16:47:05
     * @Description:
     * http://localhost:8085/tx1/txtest/save?id=1&value=22
     */
    @RequestMapping(value = "/save")
    public Response save(TxTest bean) {
        //IUser user = UserContext.getUserContext().getUser();
        int res = 0;
        if (StringUtil.isNotEmpty(bean.getId())) {
            // 主键不为空, 修改数据
             res = txTestService.update(bean);
        } else {
            //主键为空, 新增数据
            bean.initCreate("");
            res = txTestService.add(bean);
        }
        return returnSuccess(res);
    }

}
