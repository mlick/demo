package com.sesame.txtest.feign;


import com.sesame.framework.tx.config.TxFeignConfig;
import com.sesame.framework.web.response.Response;
import com.sesame.framework.web.response.ResponseFactory;
import com.sesame.txtest.bean.TxTest;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = FeignServiceTx2.APP_NAME, configuration = TxFeignConfig.class,fallback = FeignServiceTx2Hystrix.class)
public interface FeignServiceTx2 {

    String APP_NAME = "eureka-tx2";
    String SERVICE_NAME = "tx2";

    @GetMapping(SERVICE_NAME + "/txtest/listAll")
    Response<List<TxTest>> getList();

    @RequestMapping(value = SERVICE_NAME + "/txtest/update2", method = RequestMethod.POST)
    Response update2(@RequestParam("id") String id, @RequestParam("value") String value);

}

@Component
class FeignServiceTx2Hystrix implements  FeignServiceTx2{

    @Override
    public Response<List<TxTest>> getList() {
        return ResponseFactory.feignException("getList");
    }

    @Override
    public Response update2(String id, String value) {
        return ResponseFactory.feignException("update2");
    }
}