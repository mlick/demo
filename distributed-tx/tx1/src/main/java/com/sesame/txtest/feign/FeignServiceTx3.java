package com.sesame.txtest.feign;


import com.sesame.framework.tx.config.TxFeignConfig;
import com.sesame.framework.web.response.Response;
import com.sesame.framework.web.response.ResponseFactory;
import com.sesame.txtest.bean.TxTest;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = FeignServiceTx3.APP_NAME, configuration = TxFeignConfig.class,fallback = FeignServiceTx3Hystrix.class)
public interface FeignServiceTx3 {

    String APP_NAME = "eureka-tx3";
    String SERVICE_NAME = "tx3";

    @RequestMapping(value = SERVICE_NAME + "/txtest/update3", method = RequestMethod.POST)
    Response update3(@RequestParam("id") String id, @RequestParam("value") String value);

}
@Component
class FeignServiceTx3Hystrix implements  FeignServiceTx3{

    @Override
    public Response update3(String id, String value) {
        return ResponseFactory.feignException("update3");
    }
}