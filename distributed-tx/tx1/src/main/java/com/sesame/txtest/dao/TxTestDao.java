package com.sesame.txtest.dao;

import org.apache.ibatis.annotations.Mapper;
import com.sesame.txtest.bean.TxTest;

import java.util.List;

/**
 * TxTestDao
 * @author admin
 * @date 2017-12-21 16:47:05
 * @Description: 分布式事物测试表
 */
 @Mapper
public interface TxTestDao {

	/** 新增  */
	int insert(TxTest bean);
	
	/** 删除  */
	int delete(String id);
	
	/** 修改  */
	int update(TxTest bean);
	
	/** 查询所有 */
	List<TxTest> selectList(TxTest bean);
	

}
