## 创建一个HTTPS网站
### 什么是https？
>HTTPS（全称：HyperText Transfer Protocol over Secure Socket Layer），其实 HTTPS 并不是一个新鲜协议，Google 很早就开始启用了，初衷是为了保护数据安全。 近两年，Google、Baidu、Facebook 等这样的互联网巨头，不谋而合地开始大力推行 HTTPS， 很多国内外的大型互联网公司也都已经启用了全站 HTTPS，这也是未来互联网发展的趋势。
>为鼓励全球网站的 HTTPS 实现，Google 甚至调整了搜索引擎算法，让采用 HTTPS 的网站在搜索中排名更靠前。并且从 2017 年开始，Chrome 浏览器已把采用 HTTP 协议的网站标记为不安全网站，苹果 App Store 中的所有应用也都必须使用 HTTPS 加密连接；当前国内炒的很火热的微信小程序也要求必须使用 HTTPS 协议；新一代的 HTTP/2 协议的支持需以 HTTPS 为基础。因此想必在不久的将来，全网 HTTPS 势在必行。但是，如果网站想使用 HTTPS 服务，则必须为域名从数字证书授权机构（CA，Certificate Authority）申请相关的 SSL 证书。
> 证书有好多种，加密算法也各有不通，比如有Symantec、GeoTrust、TrustAsia、Let’s Encrypt 的各类 DV/OV/EV SSL 证书的申购服务，其中 TrustAsia、Let’s Encrypt DV SSL 单域名证书可免费申请。
> 下面将会讲讲解下如何在linux环境在创建免费的证书。
### linux环境下创建Let’s Encrypt免费证书

    1. 安装Let's Encrypt前的准备工作
   >  根据官方的要求，我们在VPS、服务器上部署Let's Encrypt免费SSL证书之前，需要系统支持Python2.7以上版本以及支持GIT工具。并需要把域名解析到服务器所在的主机IP中。还有需要开启443端口，不然会报连不到服务器。

    2.快速获取Let's Encrypt免费SSL证书
```
git clone https://github.com/letsencrypt/letsencrypt
cd letsencrypt
./letsencrypt-auto certonly --standalone --email 956217275@qq.com -d test1.mlick.com
```

    3.Let's Encrypt免费SSL证书获取与应用
```
在完成Let's Encrypt证书的生成之后，我们会在"/etc/letsencrypt/live/laozuo.org/"域名目录下有4个文件就是生成的密钥证书文件。
cert.pem  - Apache服务器端证书
chain.pem  - Apache根证书和中继证书
fullchain.pem  - Nginx所需要ssl_certificate文件
privkey.pem - 安全证书KEY文件
```

```
在Nginx环境，那就需要用到fullchain.pem和privkey.pem这两个证书文件，然后在nginx.conf文件中配置https支持，并把证书地址配置到文件中即可
ssl_certificate /etc/letsencrypt/live/test1.mlicl.com/fullchain.pem;
ssl_certificate_key /etc/letsencrypt/live/test1.mlicl.com/privkey.pem;

```

    4.解决Let's Encrypt免费SSL证书有效期问题
```
文件的证书有效期是90天，到期时可以自己续期即可，当然可以做个定时任务去执行，每月的1号去重新获取
./letsencrypt-auto certonly --renew-by-default --email 956217275@qq.com -d test1.mlick.com
```
