package net.wjh.controller;

import com.sesame.framework.web.controller.AbstractWebController;
import com.sesame.framework.web.response.Response;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/index")
public class IndexCtl extends AbstractWebController {

    @RequestMapping("/test")
    public Response test(String name){
        return returnSuccess(name);
    }
}
