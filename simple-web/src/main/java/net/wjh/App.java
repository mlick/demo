package net.wjh;

import com.sesame.framework.web.context.SpringContextUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * 项目启动时, 运行这个main方法就好
 */
@ServletComponentScan(basePackageClasses = com.sesame.framework.web.init.SystemStart.class)
@SpringBootApplication
public class App {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(App.class, args);

        /**
         * 可删除下面这段代码
         */
        SpringContextUtil.setApplicationContext(context);
        SpringContextUtil.println();

    }

}
